﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using probaSito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace probaSito.Tests
{
    [TestClass()]
    public class ProgramTests
    {

        /// <summary>
        /// Test sito method from main class
        /// This should check if values are correct
        /// </summary>
        [TestMethod()]
        public void SitoTest()
        {
            Assert.AreEqual(1,1);
        }
    }
}