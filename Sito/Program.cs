﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace probaSito
{

    /// <summary>
    /// Main class of application.
    /// </summary>
    public class Program
    {

        /// <summary>
        /// Public method Sito
        /// Returns array M elements specifying the consecutive answers to all queries
        /// </summary>
        /// <param name="tablica"></param>
        /// <param name="zakres"></param>
        /// <param name="dokad"></param>
        static public void Sito(int[] tablica, int zakres, int dokad)
        {
            for (int i = 2; i <= dokad; i++)
            {
                if (tablica[i] != 0)
                {
                    int j = i + i;
                    while (j <= zakres)
                    {
                        tablica[j] = 0;
                        j += i;
                    }
                }
            }

        }


        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            int i, j, zakres, dokad;
            int[] tablica = new int[10000];

            Console.WriteLine("Top:");
            zakres = int.Parse(Console.ReadLine());
            dokad = (int)Math.Floor(Math.Sqrt(zakres));

            for (i = 1; i <= zakres; i++) tablica[i] = i;

            Sito(tablica, zakres, dokad);
            Console.WriteLine("Primes to " + zakres);
            for (i = 2; i <= zakres; i++)
                if (tablica[i] != 0)
                    Console.WriteLine(i + ", ");

            Console.ReadKey();
        }
    }
}
